package com.imatia.pruebatecnica.controllers;

import com.imatia.pruebatecnica.model.dto.OrderTrackingListDTO;
import com.imatia.pruebatecnica.model.service.OrderTrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.imatia.pruebatecnica.model.dto.OrderTrackingDTO;

import java.util.List;

@RestController
public class OrderController {
	@Autowired
	private OrderTrackingService trackingService;


	@PostMapping("/order/tracking")

	public ResponseEntity<?> postTracking(@RequestBody OrderTrackingListDTO orders) {

		try {
			OrderTrackingListDTO result = trackingService.PostState(orders);
			return ResponseEntity.status(HttpStatus.OK).body(result);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


}
