package com.imatia.pruebatecnica.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.imatia.pruebatecnica.model.dto.OrderTrackingListDTO;
import com.imatia.pruebatecnica.model.entity.Order;
import com.imatia.pruebatecnica.model.entity.OrderState;
import com.imatia.pruebatecnica.model.repository.OrderRepository;
import com.imatia.pruebatecnica.model.repository.OrderStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imatia.pruebatecnica.model.dto.OrderTrackingDTO;

@Service
public class OrderTrackingServiceImpl implements OrderTrackingService{
	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderStateRepository orderStateRepository;

	@Autowired
	private StateMachineService stateMachineService;

	public OrderTrackingServiceImpl() {
		super();
	}

	public OrderTrackingServiceImpl(OrderRepository orderRepository) {
		super();
		this.orderRepository = orderRepository;
	}

	@Override
	public OrderTrackingListDTO PostState(OrderTrackingListDTO estados) {
		OrderTrackingListDTO result = new OrderTrackingListDTO();
		List<OrderTrackingDTO> orderTrackings = result.getOrderTrackings();

		for (OrderTrackingDTO orderTrackingDTO : estados.getOrderTrackings()) {
			Optional<Order> order = orderRepository.findById(orderTrackingDTO.getOrderId());
			if (!order.isPresent()) {
				Order newOrder = new Order();
				newOrder.setId(orderTrackingDTO.getOrderId());
				newOrder.setCurrentStateId(orderTrackingDTO.getTrackingStatusId());
				newOrder = orderRepository.save(newOrder);
				OrderState os = new OrderState();
				os.setOrderId(newOrder.getId());
				os.setStateId(orderTrackingDTO.getTrackingStatusId());
				os.setStateUpdateDate(orderTrackingDTO.getChangeStatusDate());
				os = orderStateRepository.save(os);
				newOrder.getStates().add(os);
				orderTrackings.add(new OrderTrackingDTO(newOrder.getId(),
						os.getStateId(),
						os.getStateUpdateDate()));
			}
			else {
				Comparator orderStateUpdateDateComparator = new Comparator<OrderState>() {
					@Override
					public int compare(OrderState o1, OrderState o2) {
						return o2.getStateUpdateDate().compareTo(o1.getStateUpdateDate());
					}
				};
				Optional<OrderState> currentState = order.get()
														.getStates()
														.stream()
														.sorted(orderStateUpdateDateComparator)
														.findFirst();
				if (!currentState.isPresent() || stateMachineService.isValidTransition(currentState.get().getStateId(),
														orderTrackingDTO.getTrackingStatusId())) {
						OrderState os = new OrderState();
						os.setStateUpdateDate(orderTrackingDTO.getChangeStatusDate());
						os.setStateId(orderTrackingDTO.getTrackingStatusId());
						os.setOrderId(orderTrackingDTO.getOrderId());
						os = orderStateRepository.save(os);

						order.get().getStates().add(os);
						orderTrackings.add(new OrderTrackingDTO(order.get().getId(),
																os.getStateId(),
																os.getStateUpdateDate()));
				}
				else {
					if(currentState.isPresent())  {
						OrderTrackingDTO otdto = new OrderTrackingDTO();
						otdto.setChangeStatusDate(currentState.get().getStateUpdateDate());
						otdto.setOrderId(currentState.get().getOrderId());
						otdto.setTrackingStatusId(currentState.get().getStateId());
						orderTrackings.add(otdto);
					}
					else orderTrackings.add(orderTrackingDTO);
				}
			}
		}
		return result;
	}
	
	
}
