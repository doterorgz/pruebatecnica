package com.imatia.pruebatecnica.model.service;


import com.imatia.pruebatecnica.model.dto.OrderTrackingDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface ProcessFileService {


    OrderTrackingDTO processFile(MultipartFile file) throws IOException;


}
