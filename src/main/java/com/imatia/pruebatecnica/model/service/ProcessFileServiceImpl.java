package com.imatia.pruebatecnica.model.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.imatia.pruebatecnica.model.dto.OrderTrackingDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Service
public class ProcessFileServiceImpl implements ProcessFileService {

    @Override
    public OrderTrackingDTO processFile(MultipartFile multipartFile) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        OrderTrackingDTO orderTrackingDTO = objectMapper.readValue(convert(multipartFile), OrderTrackingDTO.class);

        return orderTrackingDTO != null ? orderTrackingDTO : null;
    }



    private File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        try(InputStream is = file.getInputStream()) {
            Files.copy(is, convFile.toPath());
        }
        return convFile;
    }
}
